package GameStates;

import MainProzor.IgraPanel;

import java.awt.*;

/**
 * Created by Mario Tucak on 20.5.2017..
 * Stanje igre koje se prikazuje odnosno iscrtava ne ekran
 * kada je igrica pauziran,
 * U klasi definiramo boje elementa odnosno pauza znaka, i radius x i y-a
 * te grafički elemen Graphics2D koji crta te elemente na ekran
 */
public class PauseState {
    private Color pauseColor;
    private int rx;
    private int ry;
    private Graphics2D g;

    /**
     * postavlja se boja i širina elemenata i visina tih elemenata
     * @param g Graphics2D iz IgraPanela koji crta grafiku na ekran
     */
    public PauseState(Graphics2D g){
        this.g=g;
        pauseColor=Color.white;
        rx=50;
        ry=400;
    }

    /**
     * Crtaju se dva pravoktnika određene boje u konstruktoru, postavlja se
     * obrub malo crnije od uzete boje
     */
    public void start(){
        g.setColor(pauseColor);
        g.fillRect((IgraPanel.sirina-rx*4)/2,(IgraPanel.visina-ry)/2,rx,ry);
        g.setStroke(new BasicStroke(3));
        g.setColor(pauseColor.darker());
        g.drawRect((IgraPanel.sirina-rx*4)/2,(IgraPanel.visina-ry)/2,rx,ry);
        g.setStroke(new BasicStroke(1));
        g.setColor(pauseColor);
        g.fillRect((IgraPanel.sirina+rx*3)/2,(IgraPanel.visina-ry)/2,rx,ry);
        g.setStroke(new BasicStroke(3));
        g.setColor(pauseColor.darker());
        g.drawRect((IgraPanel.sirina+rx*3)/2,(IgraPanel.visina-ry)/2,rx,ry);
        g.setStroke(new BasicStroke(1));
    }
}
