package GameStates;

import MainProzor.IgraPanel;

import java.awt.*;

import static MainProzor.IgraPanel.player;

/**
 * Created by Mario Tucak on 20.5.2017..
 * GameOver stanje igre koja se pokreće kada je igra završena
 * u kojoj se definiraju
 * boja pozadine, boja teksta , font
 * score i grafički elementi
 */
public class GameOver {
    private  String GameOver;
    private Color backgroundColor;
    private Color text;
    private int fontSize;
    private String font;
    private String score;
    private Graphics2D g;

    /**
     * Postavlja se vrijednost string GameOver
     * ,postavlja se određeni font
     * ,postavlja se boja pozadine
     * ,postavlja se boja teksta
     * ,postavlja se velicina fonta i pridružuje se referenca na grafičku iz igraPanela
     * @param g Graphics2D od IgraPanela za crtanje grafike
     */
    public GameOver(Graphics2D g){
        GameOver="G A M E  O V E R";
        font="Century Gothic";
        backgroundColor=Color.BLACK;
        text=Color.WHITE;
        fontSize=30;
        this.g=g;

    }

    /**
     * Crta se pozadia crne boje u obliku pravokutnika dimenzija IgraPanel.sirna,IgraPanel.visina,
     * postavlja se boja teksta i font, i fontSize,te se crita taj tekst na sredini ekrana
     * pridružuje se Stringu Score ime SCORE I brojčana vrijednost iz IgrePanela.player.getScore()
     * ,postavlja se boja i font za taj tekst te se crta na poziciji ispod sredine ekrana
     */
    public void start(){
        g.setColor(backgroundColor);
        g.fillRect(0,0,IgraPanel.sirina, IgraPanel.visina);
        g.setColor(text);
        g.setFont(new Font(font,Font.BOLD,fontSize));
        g.drawString(GameOver,(IgraPanel.sirina-175)/2,IgraPanel.visina/2);
        score="S C O R E :"+IgraPanel.player.getScore();
        g.setColor(text);
        g.setFont(new Font(font,Font.BOLD,fontSize));
        g.drawString(score,(IgraPanel.sirina-150)/2,(IgraPanel.visina/2)+(IgraPanel.visina/8));
    }
}
