package GameStates;

import MainProzor.IgraPanel;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by Mario Tucak on 20.5.2017..
 * Nije Panel u java smislu već je samo slika nacrtana od grafičkih elemenata na IgraPanelu
 * prije nego što se se sama igra pokrene,
 * U njemu su definirane sve potrebne stvari za objašnjavanje pravila igre
 */
public class StartGamePanel {
    private String movements;
    private String shoot;
    private String pause;
    private String cilj;
    private String font;
    private String pocetIgru;
    private Color textColor;
    private Color rectColor;
    private Graphics2D g;
    private int razmak;
    private int fontSize;

    /**
     * Postavlja se font na 18 veličinu, boja teksta na crnu, boja pravkokutnika koji ce biti crtna na
     * narančastu,određeni tekst movements="KRETNJE -- STRELICE --";
      shoot="PUCANJE -- W --";
      pause="PAUZA -- ESC --";
      cilj="POSTAVITE NEPRIJATELJE NA ZADANU BOJU";
      pocetIgru="ZA POCET IGRU PRITISNITE -- ENTER --";
     i font na Century Gothic, te razmak između tekstova
     *
     * @param g Graphics2D iz IgraPanela koji crta grafiku na ekran
     */
    public StartGamePanel(Graphics2D g){
        this.g=g;
        fontSize=18;
        textColor= Color.BLACK;
        rectColor= Color.ORANGE;
        movements="KRETNJE -- STRELICE --";
        shoot="PUCANJE -- W --";
        pause="PAUZA -- ESC --";
        cilj="POSTAVITE NEPRIJATELJE NA ZADANU BOJU";
        pocetIgru="ZA POCET IGRU PRITISNITE -- ENTER --";
        font="Century Gothic";
        razmak=30;

    }

    /**
     * Crta 2 velika pravokutnika prvi veličine IgraPanel.sirina*IgraPanel.visina
     * , drugi je pak veličine 525*525 i starta na sirina/16 kordinatama x  i visina/16 kordinatama y
     * Unutar manjeg pravokutnika se piše tekst s razmakom *2/4/6/8
     */
    public void start(){

        g.setColor(Color.WHITE);
        g.fillRect(0,0, IgraPanel.sirina,IgraPanel.visina);
        g.setColor(rectColor);
        g.fillRect(IgraPanel.sirina/16,IgraPanel.visina/16,
                525,525);
        g.setStroke(new BasicStroke(3));
        g.setColor(rectColor.darker());
        g.drawRect(IgraPanel.sirina/16,IgraPanel.visina/16,
                525,525);
        g.setStroke(new BasicStroke(1));
        g.setColor(textColor);
        g.setFont(new Font(font,Font.BOLD,fontSize));
        g.drawString(cilj,IgraPanel.sirina/8,(IgraPanel.visina/8));

        g.setColor(textColor);
        g.setFont(new Font(font,Font.BOLD,fontSize));
        g.drawString(movements,IgraPanel.sirina/8,(IgraPanel.visina/8)+razmak*2);

        g.setColor(textColor);
        g.setFont(new Font(font,Font.BOLD,fontSize));
        g.drawString(shoot,IgraPanel.sirina/8,(IgraPanel.visina/8)+razmak*4);

        g.setColor(textColor);
        g.setFont(new Font(font,Font.BOLD,fontSize));
        g.drawString(pause,IgraPanel.sirina/8,(IgraPanel.visina/8)+razmak*6);

        g.setColor(textColor);
        g.setFont(new Font(font,Font.BOLD,fontSize));
        g.drawString(pocetIgru,IgraPanel.sirina/8,(IgraPanel.visina/8)+razmak*8);


    }

}
