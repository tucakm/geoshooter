package MainProzor;

import Baza.BazaFile;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

/**
 * Created by Mario Tucak on 21.5.2017..
 * Main klasa metode sa grafičkim sučeljem
 * proširuje JFrame i nasljeđuje sve njegove osobnosti
 */
public class IgraMain extends JFrame {
    private JPanel cardLayout;
    private JPanel usernamePanel;
    private JPanel menuPanel;
    private JPanel highScorePanel;
    private JTextField usernametextfield;
    private JButton nextbutton;
    private JButton newGameButton;
    private JButton highScoreButton;
    private JButton quitButton;
    private JButton nazad;
    private JLabel highscoresLabel;
    private JTextArea textArea1;

    private Igra game;
    int randomLevel;
    private Map<String,Integer> highScore;
    static BazaFile b;

    /**
     * Konstruktor klase koji postavlja ime, čita podatke iz baze,
     * određuje koji layout je prikazan pomoću adciton listenera na  buttonima
     * randomLevel varijabla određuje random broj od 0 do 999 i pomocu njega
     * određujemo koji je level ce nam program ucitati
     * definiramo actionListenere koji imaju ulogu pokrećanja određenih panela i same igre koja je također panel
     *
     */
     public IgraMain() {
        super("GeoShooter");
        cardLayout.setVisible(true);


        IgraMain.b = new BazaFile();highScore= IgraMain.b.citaj();
        randomLevel=(int)(Math.random()*1000);
        game = new Igra();
        nextbutton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                System.out.println(randomLevel);
                JOptionPane.showMessageDialog(null, "Welcome TO GeoShooter");
                menuPanel.setVisible(true);
                usernamePanel.setVisible(false);


            }
        });
        newGameButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                randomLevel=(int)(Math.random()*2);
                game.start();
                try {
                    IgraPanel.player.setUsername(usernametextfield.getText());
                }catch(Exception e1){
                    JOptionPane.showMessageDialog(null,"Nije upisan username");
                }
                setVisible(false);
            }
        });
        highScoreButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                menuPanel.setVisible(false);
                highScorePanel.setVisible(true);
                ArrayList<Integer> scoreList= new ArrayList<>(highScore.values());
                Collections.sort(scoreList);
                Collections.reverse(scoreList);
                StringBuilder sb = new StringBuilder();
                for(int i=0;i<scoreList.size();i++){
                                sb.append(i);
                                sb.append("---SCORE:");
                                sb.append(scoreList.get(i));
                                sb.append("\n");
                }

                textArea1.setText(sb.toString());
            }
        });
        quitButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });


        nazad.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                menuPanel.setVisible(true);
                highScorePanel.setVisible(false);
            }
        });
    }

    /**
     * Main metoda definira IgraMain koja poreće prozor sa svim komponenatama
     * @param args
     */
    public static void main(String[] args) {
        IgraMain window1 = new IgraMain();
        window1.init();
    }

    private void init() {
        setSize(300,300);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setContentPane(cardLayout);
        cardLayout.setPreferredSize(new Dimension(400,400));
        setResizable(true);
        setBounds(600, 300, IgraPanel.sirina, IgraPanel.visina);
        pack();
        setVisible(true);
    }


}
