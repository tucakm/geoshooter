package MainProzor;

import GameStates.GameOver;
import GameStates.PauseState;
import GameStates.StartGamePanel;
import Igrac.Bullet;
import Igrac.Player;
import Leveli.Level;
import Leveli.LevelA;
import Leveli.LevelB;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Created by Mario Tucak on 6.5.2017..
 * IgraPanel sluzi ko mozak igre, proširuje JPanel,imprementamo runnable
 * koji omogucuje izvođenje programa preko dretve, dok KeyListener sluzži za detektiranje
 * određenih tipaka sa tipkovnice.
 * Sav code vodi do IgraPanela i izvodi se u njemu
 */
public class IgraPanel extends JPanel implements Runnable , KeyListener  {

    //Dimenzije
    public static final int sirina=600;
    public static final int visina=600;

    //objekti
    public static Player player;
    public static ArrayList<Bullet> bullets;

    //Dretva
    private Thread dretva;

    //stanje u kojoj je igra
    private boolean running;
    private boolean startGame;
    private boolean paused;

    //slika
    private BufferedImage slika;
    private Graphics2D g;

    //FPS I Zeljeno vrijeme izvođenja
    private int FPS;
    private long targetTime;

    //Leveli
    private Level rectLevel= new LevelA();
    private Level circleLevel=new LevelB();
    //GameStates
    private GameOver gameOver;
    private PauseState pausedState;
    private StartGamePanel startGamePanel;
    private IgraMain menu;

    private int pauseSetter;

    /**
     * Konstruktor određuje dimenzije i postavlja panel na focus,
     * dohvaća imput s panela i postavljakey listener na IgraPanel
     */
    public IgraPanel(){
        setPreferredSize(new Dimension(sirina,visina));
        setFocusable(true);
        requestFocus();
        addKeyListener(this);
    }
    //notificira da li je upaljen prozor
    public void addNotify(){
        super.addNotify();
        if(dretva==null){
            dretva= new Thread(this,"GeoShoter");
            dretva.start();
        }
    }

    private void init(){
        running=true;
        FPS=60;
        targetTime=1000/FPS;
        slika=new BufferedImage(sirina,visina,BufferedImage.TYPE_INT_RGB);
        g=(Graphics2D) slika.getGraphics();
        /*Za glađe učitavanje grafike*/
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        player=new Player();
        bullets= new ArrayList<>();
        gameOver=new GameOver(g);
        pausedState=new PauseState(g);
        startGamePanel=new StartGamePanel(g);

        paused=false;
        pauseSetter=0;
        startGame=false;
        menu=new IgraMain();

    }

    /**
     * Metoda povezana dretvom, pokreće se paljenjem programa
     * U njoj se nalazi gameloop koji se izvodi tijekom cijele igre
     */
    public void run(){
        init();
        /*pocetno vrijeme, proslo vrijeme nakon 1 izvođenja gameUpdate();
        gameRender(),gameDraw()i vrijeme spavanja odnosno cekanja podnovnog rada dretve*/
        long start;
        long passedTime;
        long wait;
        /*Postavljanje pocetnog ekrana, prije nego sto se uđe u game petlju,
          */
        while(!startGame){
            startGamePanel.start();
            gameDraw();
        }
        /*Game LOOP koja se u obom slucaju izvodi 60 puta u sekundi
        u njoj se nalaze glavne funkcije za izvođenje logike i slike na ekran
        * */
        while(running) {
            /*
            /Pocetno vrijeme svaki put kad se game loop vrati na start
             */
            start = System.nanoTime();

            gameUpdate();
            gameRender();
            gameDraw();

            /*Oduzmemo trenutno vrijeme sa vremenom startanja i dobijemo
            proslo vrijeme od izvođenja svih game funkcija,
            zatim se zeljeno vrijeme koje je prikazano u milliseconds oduzme s vremenom proslim koje je u
            nanosekundama zbog vece preciznostii dobijemo vrijeme koje dretva odspava svaki put kad se game loop izvodi
            * */
            passedTime = System.nanoTime() - start;
            wait = targetTime - passedTime / 1000000;
            if(wait < 0)
                wait = 5;

            try {
                Thread.sleep(wait);
            }
            catch(Exception e) {
                e.printStackTrace();
            }

        }
        /*Kad izademo iz game loopa to znaci da smo gotovi s igrom, i virjeme je da se
        spreme podatci u bazu podataka
        * */
        menu.b.dodaj(player.score,player.getUsername());

        /*pokreće se gameOverStanje i crta se grafika koja je definirana u gameOver.statu();
        * */
        gameOver.start();
        gameDraw();
    }

    /*Nije kao što ime govori da puca, vec provjerava da li je metak prešao granice ekrana
    (bullets.get(i).update) u slučaju da je izbriše se i time se ne puni memorija
    * */
   private void shoot(){
       for(int i=0;i<bullets.size();i++){
           boolean remove= bullets.get(i).update();
           if(remove){
               bullets.remove(i);
               i--;
           }
       }
   }
   private void drawBullets(){
       for(int i=0;i<bullets.size();i++){
           bullets.get(i).draw(g);
       }
   }

   /*Funkcija koja je zaslužna za svu logiku u igru*/
    private void gameUpdate(){
        if(menu.randomLevel%2==0){
            rectLevel.update();}
        else{
        circleLevel.update();
        }

        player.update();
        shoot();

        if(player.getZivoti()<=0){
            running=false;
        }
        pausedState();

    }

    /*Glavna funkcija za anžuriranje slike*/
    private void gameRender(){
        if(menu.randomLevel%2==0){
            rectLevel.draw(g);}
        else{
            circleLevel.draw(g);
        }

        player.draw(g);
        drawBullets();

    }

    /*Glavna funkcija za crtanje na ekran*/
    private void gameDraw(){
        Graphics g2 = this.getGraphics();
        g2.drawImage(slika, 0, 0, null);
        g2.dispose();
    }
    /*provjerava da li je pauzirana igra
    * */
    private void pausedState(){
        while(paused){
            pausedState.start();
            gameDraw();
        }
    }
    /*algoritam koji provjerava koliko puta je pritisnuta tipka ESC i
    pomoću toga određuje jel pauzirano stanje ili ne
    * */
    private void checkPressedTime(int i){
        pauseSetter+=i;
        if(pauseSetter%2==1){
            paused=true;
        }
        if(pauseSetter%2==0){
            paused=false;
        }
    }

    public void keyTyped(KeyEvent e) {
    }

    /**
     * Provjerava da li su pritnisnute određene tipke, imprementirana funkcija iz KeyListenera
     * prepoznaje kod određenih tipaka u slučaju kada su pritisnuti, postavlja određene boolean varijable na ture
     * @param e
     */
    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode()==KeyEvent.VK_UP)
            player.setUp(true);
        else if(e.getKeyCode()==KeyEvent.VK_DOWN)
            player.setDown(true);
        else if(e.getKeyCode()==KeyEvent.VK_LEFT)
            player.setLeft(true);
        else if(e.getKeyCode()==KeyEvent.VK_RIGHT)
            player.setRight(true);
        else if(e.getKeyCode()==KeyEvent.VK_W)
            player.setFiring(true);
        else if(e.getKeyCode()==KeyEvent.VK_ESCAPE) {
            checkPressedTime(1);
        }

    }

    /**
     * Provjerava da li su puštene određene tipke, imprementirana funkcija iz KeyListenera
     * prepoznaje kod određenih tipaka u slučaju kada su puštene, radi određene mogućnosti
     * odnosno postavlja određene boolean varijable na false
     * @param e
     */
    public void keyReleased(KeyEvent e) {
        if(e.getKeyCode()==KeyEvent.VK_UP)
            player.setUp(false);
        else if(e.getKeyCode()==KeyEvent.VK_DOWN)
            player.setDown(false);
        else if(e.getKeyCode()==KeyEvent.VK_LEFT)
            player.setLeft(false);
        else if(e.getKeyCode()==KeyEvent.VK_RIGHT)
            player.setRight(false);
        else if(e.getKeyCode()==KeyEvent.VK_W)
            player.setFiring(false);
        else if(e.getKeyCode()==KeyEvent.VK_ENTER) {
            startGame=true;
        }

    }


}
