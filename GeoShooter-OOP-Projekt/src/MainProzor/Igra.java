package MainProzor;

import javax.swing.*;

/**
 * Created by Mario Tucak on 6.5.2017..
 * Klasa kojoj je funkcija stvaranje prozora za IgraPanel
 */
public class Igra {

     void start(){
        IgraPanel igra=new IgraPanel();
        JFrame prozor = new JFrame("GeoShooter");
        prozor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        prozor.setContentPane(igra);
        prozor.setResizable(false);
        prozor.setBounds(300,50,IgraPanel.sirina,IgraPanel.visina);
        prozor.pack();
        prozor.setVisible(true);
    }
}
