package Leveli;

import java.awt.*;
import java.util.ArrayList;
import Enemy.Enemy;
import Igrac.Bullet;
import MainProzor.IgraPanel;
/**
 * Created by Mario Tucak on 8.5.2017..
 * Apstraktna klasa koja sadrži sve metode što ce svaki od levela sadržati
 * Sadrži dvije static liste neprijatelja  , i grafičke elemente
 * u njoj se sadrži metode za detektiranje sudara neprijatelja i metka i neprijatelja i palyera
 * i ostalih specifičnosti igre, tipa timer između valova, ukalnjanje neprijatelja, crtanje zadanih boja
 * određivanje zadanih boja, ispisivanje dead teksta prikikom uklanjanja neprijatelja
 */
public abstract class Level {

    static ArrayList<Enemy> enemies =new ArrayList<>();
    static ArrayList<Enemy> enemiesColor =new ArrayList<>();
    static ArrayList<MoreGraphicElements> elem=new ArrayList<>();
    static int waveNumber;
    static Color killingColor;

    private long waveStart;
    private int waitTime;
    private boolean start;
    private long waitTimeBetweenWaves;
    private Color color;
    private String GoalColor;

    /**
     * postavlja se broj vala na 0,
     * pocetak timera vala na 0,
     * cekanje između valova na0
     * , početak na false
     * i vrijeme čekanja na 1.5 sekundi
     * i string GoalColor na Zadana Boja:
     */
    public Level(){
        waveNumber =0;
        waveStart =0;
        waitTimeBetweenWaves =0;
        start =false;
        waitTime=1500;
        GoalColor="Z A D A N A  B O J A:";
    }

    /**
     * čita svaki metak iz IgraPanela i njegove x i y kordinate i radius
     * čita svaki neprijatelj i njegove x y kordinate,
     * računa dx i dy koji je jednak bulletx- enemx odnostno bullety-enemy,
     * računa udaljenost pomoću pitagorina poučka,
     * i provjerava da li je udaljenost svakog metka manja od radiusa metka+ radiusa neprijatelja,
     * u slučaju da je  poziva se funkcija hit iz interfacea enemy koja mijenja boju, i uklanja se metak iz
     * arraylista bullets u igra panelu
     */
    void bulletEnemyCollision(){
        for(int i = 0; i< IgraPanel.bullets.size(); i++){
            Bullet bullet= IgraPanel.bullets.get(i);
            double bulletX=bullet.getX();
            double bulletY=bullet.getY();
            double bulletR=bullet.getR();
            for(int j = 0; j< enemies.size(); j++){
                Enemy e= enemies.get(j);
                double ex=e.getX();
                double ey=e.getY();
                double er=e.getR();
                double dx=bulletX-ex;
                double dy=bulletY-ey;
                double dist=Math.sqrt(dx*dx+dy*dy);// udaljenost

                if(dist<bulletR+er){

                    IgraPanel.bullets.remove(i);
                    e.hit();
                    i--;
                    break;
                }
            }
        }
    }

    /**
     * Jako slična funkcija funkciji bulletEnemyCollision, samo što postoji jedan player
     * i provjerava da li je igrac vec pogoden, odnosno jel se aktivira timer nemogućnosti da se naudi playeru
     * u slučaju da nije uzimaju se x y i r te se prolazi kroz for petlju da de uzme svaki neprijatelj
     * te računa se udaljenost između neprijatelja i playera,
     * u slučaju da je udaljenost manjja od veličine player i veličine neprijatelja
     * poziva se funkicja hitted koja je definirana u playeru i onda zapravo postavlja playera
     * na stanje nemogućnosti primanja dmg
     */
    void playeEnemyCollision() {
        if (!IgraPanel.player.isPogoden()) {
            int playerx = IgraPanel.player.getX();
            int playery = IgraPanel.player.getY();
            int playerr = IgraPanel.player.getR();
            for (int i = 0; i < enemies.size(); i++) {
                Enemy e = enemies.get(i);
                double ex = e.getX();
                double ey = e.getY();
                double er = e.getR();
                double dx = playerx - ex;
                double dy = playery - ey;
                double dist = Math.sqrt(dx * dx + dy * dy);// udaljenost

                if (dist < playerr + er) {
                    IgraPanel.player.hitted();
                }
            }
        }
    }

    /**
     * addTimer ima ulogu postavljanja timera između svakog vala,
     * u njemu se provjerava da li je arraylist enemies jednak nulli i wave start nulla
     * ako je, poveca se val , postavi se boja koja je cilj i start se postavi na false,
     * i timer se postavi
     * u elsu se postavi vrijednost u waitTimeBetweenWaves= (System.currentTimeMillis()- waveStart),
     * koja definira koliko je vremena proslo, i kada je prolo vise od waitTime-a koje je definiran 1.5sekundi
     * elementi tipa dead text se brišu, start se postavlja na istinito,
     * waveStart se ponovno postavlja na nulu i waitTimeBeetweenWaves se postavlja na nulu
     * i time se dobije timer između svakog vala
     */
     void addTimer(){
        if(enemies.size()==0&& waveStart ==0){
            waveNumber++;
            killingColor= getKillingColor();
            start =false;
            waveStart =System.currentTimeMillis();
        }
        else{
           waitTimeBetweenWaves =(System.currentTimeMillis()- waveStart);
            if(waitTimeBetweenWaves > waitTime){
                elem.clear();
                start =true;
                waveStart =0;
                waitTimeBetweenWaves =0;
            }
        }
    }

    public abstract void update();
    public abstract void draw(Graphics2D g);
    public abstract void addEnemy();

    /**
     * Crta boj vala u desnom gornjem kutu screena
     * @param g Graphics2D iz IgraPanela koji crta grafiku na ekran
     */
    void drawWaveNumber(Graphics2D g){
        g.setFont(new Font("Arial", Font.BOLD,15));
        String s="-- V  A  L --"+ waveNumber+"--";
        g.setColor(Color.BLACK);
        g.drawString(s,IgraPanel.sirina-s.length()-100,20);
    }

    /**
     * Pomoću random funkcije određuje koja je sljedeća killingColor tj koja boja je zadana
     * @return vraća random boju
     */
    Color getKillingColor(){
        int i=(int)(Math.random()*4)+1;

        if(i==1){
            color =Color.YELLOW;
            return color;
        }
        else if(i==2){
            color =Color.red;
            return color;
        }
        else if(i==3){
            color =Color.BLUE;
            return color;

        }
        else if(i==4){
            color =Color.GREEN;
            return color;

        }
        else
            return color;

    }

    /**
     * Uspoređuje glavnu enemy listu sa listom koja je popunjena killing colorom, zanči svi elementi su
     * te boje, i u slučaju da su iste, score se zbraja, poziva se funkcija addElem koja
     * dodaje deadText na mjesto svakog neprijatelja koji je umro i brišu se elementi iz te liste
     *
     */
    void killEnemy(){

        if(enemiesColor.equals(enemies)&& enemiesColor.hashCode()== enemies.hashCode()) {
            for (int i = 0; i < enemies.size(); i++) {
                Enemy e = enemies.get(i);
                IgraPanel.player.setScore(10 * e.getRank());
            }
            addElem();
            enemies.clear();

        }
    }

    /**
     * U slučaju da je start true i enemes lista je prazna dodaju se novi neprijatelji
     */
    void startSpawn(){
        if(start && enemies.size()==0) {
            addEnemy();
        }
    }

    /**
     * Crta se Zadana boja u gornji ljevi kut ispod života
     * @param g objekt tipa Graphics2D koji je zaduzen za sve graficke elemente i crtanja
     */
    void drawKillingColor(Graphics2D g){
        g.setColor(killingColor);
        g.fillRoundRect((GoalColor.length()+110),50,10,10,4,4);
        g.setStroke(new BasicStroke(3));
        g.setColor(killingColor.darker());
        g.drawRoundRect((GoalColor.length()+110),50,10,10,4,4);
        g.setStroke(new BasicStroke(1));
    }

    /**
     * Piše se tekst za zadanu boju u gornjem ljevom kutu
     * @param g objekt tipa Graphics2D koji je zaduzen za sve graficke elemente i crtanja
     */
    void drawMatchColorText(Graphics2D g){
        g.setFont(new Font("Courier", Font.BOLD,12));
        g.setColor(Color.BLACK);
        g.drawString(GoalColor,10,60);
    }

    /**
     * dodaju se deadText elementi iz klase MoreGraphicsElemenets u novi ArrayList
     * u ovisnosti o veličini neprijatelj liste
     */
    private void addElem(){
        for(int i=0;i<enemies.size();i++){
            elem.add(new MoreGraphicElements(enemies.get(i).getX(),enemies.get(i).getY()));
        }
    }


}