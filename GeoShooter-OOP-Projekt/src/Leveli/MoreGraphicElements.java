package Leveli;

import MainProzor.IgraPanel;

import java.awt.*;

import static Leveli.Level.elem;

/**
 * Created by Mario Tucak on 20.5.2017..
 * Služi za crtanje i logiku dead Texta u igric
 */
public class MoreGraphicElements {

    private double x;
    private double y;
    private String dead;

    /**
     * Postavlja parametre x i y na vrijednost ovog objekta x i y
     * i string na dead
     * @param x prima lokaciju na kojoj je neprijatelj umro tj x kordinatu
     * @param y prima lokaciju na kojoj je neprijatelj umro tj y kordinatu
     */
    public MoreGraphicElements(double x,double y){
        this.x=x;
        this.y=y;
        dead="DEAD";

    }

    /**
     * Postvalja vrijednost y da pada, odnosno povecava vrijednost y za 1.3 svaki put kad se izvede ova funkcija
     */
    public void update(){
        y+=1.3;
    }

    /**
     * Crta tekst na kordinatama x i y
     * @param g objekt tipa Graphics2D koji je zaduzen za sve graficke elemente i crtanja
     */
    public void draw(Graphics2D g){

            g.setFont(new Font("Arial", Font.BOLD,14));
            g.setColor(Color.WHITE);
            g.drawString(dead,(int)x,(int)y);



    }
}
