package Leveli;

import Enemy.EnemyRectangle;
import MainProzor.IgraPanel;

import java.awt.*;

/**
 * Created by Mario Tucak on 8.5.2017..
 * Proširujemo LevleA klasu sa Level apstraktnom klasom
 * zadaća ove klase je da specificira level odnosno napravi novi sa milimalnim promjenama
 * u njoj se zadaje određena boja pozadine
 */
public class LevelA extends Level {

    private Color backGroundColor;
    private int brojac;

    /**
     * Konstruktor služi za postavljanje određene boje pozadine
     */
     public LevelA() {

         backGroundColor =new Color(110, 255, 161);
         brojac=0;

    }

    /**
     *U zavisnosti na brojač dodaje se određeni neprijatelji u ovom slučaju
     * EnemyRactangle, i dodaju se različiti rankovi nepriatelja
     * I svaki put kad se pozove funkcija ponovno se isprazni lista, za provjeru
     * i na kraju se pozove metoda koja pravi novi arraylist s elementima boja killing Color,
     * u konstruktor enemyRactangle dolazi rank i random boja iz metode getKillingColor()
     */
    public void addEnemy(){
        enemies.clear();

        brojac++;
            if(waveNumber ==brojac){
                if(brojac==1) {
                    for (int i = 0; i < brojac * 2; i++) {
                        enemies.add(new EnemyRectangle(1, getKillingColor()));
                    }
                }
                else if(brojac==2) {
                    for (int i = 0; i < brojac; i++) {
                        enemies.add(new EnemyRectangle(1, getKillingColor()));

                    }
                    for (int i = 0; i < 1; i++) {
                        enemies.add(new EnemyRectangle(2, getKillingColor()));
                    }
                }
                else if(brojac==3) {
                    for (int i = 0; i < 2; i++) {
                        enemies.add(new EnemyRectangle(1, getKillingColor()));

                    }
                    for (int i = 0; i < 1; i++) {
                        enemies.add(new EnemyRectangle(2, getKillingColor()));
                    }
                    for (int i = 0; i < 1; i++) {
                        enemies.add(new EnemyRectangle(3, getKillingColor()));
                    }
                }
                else if(brojac==4) {
                    for (int i = 0; i < 2; i++) {
                        enemies.add(new EnemyRectangle(1, getKillingColor()));

                    }
                    for (int i = 0; i < 2; i++) {
                        enemies.add(new EnemyRectangle(2, getKillingColor()));
                    }
                    for (int i = 0; i < 2; i++) {
                        enemies.add(new EnemyRectangle(3, getKillingColor()));
                    }
                }
                else if(brojac==5) {
                    for (int i = 0; i < 3; i++) {
                        enemies.add(new EnemyRectangle(1, getKillingColor()));

                    }
                    for (int i = 0; i < 3; i++) {
                        enemies.add(new EnemyRectangle(2, getKillingColor()));
                    }
                    for (int i = 0; i < 2; i++) {
                        enemies.add(new EnemyRectangle(3, getKillingColor()));
                    }
                }
                else{
                    for (int i = 0; i < brojac; i++) {
                        enemies.add(new EnemyRectangle(1, getKillingColor()));

                    }
                    for (int i = 0; i < brojac; i++) {
                        enemies.add(new EnemyRectangle(2, getKillingColor()));
                    }
                    for (int i = 0; i < brojac; i++) {
                        enemies.add(new EnemyRectangle(3, getKillingColor()));
                    }

                }

                setKillColor(killingColor);
            }
    }

    /**
     * Pravi novi EnemyRACTANGLE arrayList s bojom killingColor
     * @param boja2 postavlja arraylist na određenu boju
     */
    void setKillColor(Color boja2){
        enemiesColor.clear();
        for(int i = 0; i< enemies.size(); i++){
            enemiesColor.add(new EnemyRectangle(1,boja2) );
        }
    }

    /**
     * Glavna logika iza levela, u njoj se izvoid timer između svakog levela
     * , poziva se val neprijatelja, poziva se detekcija metka i neprijatelja
     * ,na svakom neprijatelju se izvodi zasebna koja opet definira zasebne pokrete i logiku neprijatelja
     * isto tako poziva se MoreGraphicElemenets elem text koji prikazuje deadText
     * , postavlja se player i enemy sudar odnosno collision , i provjerava se da li je neprijatelja potrebno
     * uklnoiti
     */
    public  void update(){
        addTimer();
        startSpawn();
        bulletEnemyCollision();
        for(int i = 0; i< enemies.size(); i++){
            enemies.get(i).update();
        }
        if(elem.size()!=0){
            for(MoreGraphicElements e: elem){
                e.update();
            }

        }

        playeEnemyCollision();
        killEnemy();

    }

    /**
     *
     * crta pravokutnik backGroundColor boje kao pozadinu, poziva se metoda draw na svakom neprijatelju
     * pri cemu se crta svaki neprijatelj, isto tako poziva se metoda draw na svakom grafickom elemnetu
     * tj deadTextu, te crta se broj valova, zadana boja tekst te killing color
     * @param g objekt tipa Graphics2D koji je zaduzen za sve graficke elemente i crtanja
     */
    public  void draw(Graphics2D g) {
        g.setColor(backGroundColor);
        g.fillRect(0, 0, IgraPanel.sirina, IgraPanel.visina);



        for (int i = 0; i < enemies.size(); i++) {
            enemies.get(i).draw(g);
        }
        if(elem.size()!=0){
            for(MoreGraphicElements e: elem){
                e.draw(g);
            }
        }
        drawWaveNumber(g);
        drawMatchColorText(g);
        drawKillingColor(g);
    }


}


