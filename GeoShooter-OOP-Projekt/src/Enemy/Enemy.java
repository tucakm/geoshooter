package Enemy;

import java.awt.*;

/**
 * Created by Mario Tucak on 8.5.2017..
 * Drefinira osnove svkako neprijatelja, što je potrebno da bi igra funkcionirala
 *
 */
public interface Enemy {


    public double getX();
    public double getY();
    public int getR();
    public int getRank();
    public void hit();
    public void update();
    public void draw(Graphics2D g);
    public int hashCode();
    public boolean equals(Object o);

}
