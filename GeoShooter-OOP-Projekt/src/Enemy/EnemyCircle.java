package Enemy;

import MainProzor.IgraPanel;

import java.awt.*;

/**
 * Created by MarioTucak on 20.5.2017..
 * Klasa u kojoj su definirane karakteristike enemycircle, tipa kodrinate, promjena kordinata
 * , radius, kut pod kojim se krecu, brzina, dali su pogođene, boja, i rank koji određuje jačinu neprijatelja
 */
public class EnemyCircle implements Enemy {
    private double x;
    private double y;
    private int r;
    private double dx;
    private double dy;
    private double radians;
    private double speed;
    private int hitted;
    private Color color;
    private int rank;

    /**
     * Glavna ideja konsturktora je da određuje rank i boju neprijatelja, x i y kordinate
     * ,promjena vrijednosti x se nasumično mijenja od 100 do sirina -99 , a vrijednost y je postavljena
     * na -20 sto govori da se neprijatelj pojavi prvo izvan ekrana pa ide prema dolje zbog toga što je kut
     * kretnje postavljen na nasumičnu vrijednost između kuta 135 i 45,
     * promjena vrijednosti x i y odnosno dx i dy su definirane tako da se kod dx odredi cosinus kuta i pomnoži
     * sa brzinom, zbog toga što je kosinus pozitivan za vrijednosti od 270 stupnjeva do 90  što znači da
     * ce vrijednost dx biti pozitivna kad je kut smjera između ta dva kuta,
     * ista stvar je sa y-nom koji je definiram samo za sinus kojemu je pozitivan kut od 0 do 180 gledajući
     * javin kordinatni sustav koji je obrnut od kartezijevog (tipa 270= 90 u kartezijevom)
     * i postavlja se vrijednost hitted koja racuna koliko je puta pogođen neprijatelj
     * @param rank određuje koji rank neprijatelja ce se u konstruktoru stvorit
     * @param color određuje boju neprijatelja
     */
    public EnemyCircle(int rank, Color color) {
        this.rank = rank;
        this.color = color;
        if (rank == 1) {
            speed = 1;
            r = 35;
        }
        if (rank == 2) {
            speed = 1.5;
            r = 25;
        }
        if (rank == 3) {
            speed = 2;
            r = 20;
        }


        x = (Math.random() * IgraPanel.sirina - 100) + 100;
        y = -20;
        double kut = Math.random() * 135 + 45;
        radians = Math.toRadians(kut);
        dx = Math.cos(radians) * speed;
        dy = Math.sin(radians) * speed;
        hitted = 0;
    }

    public double getX() {
        return x;
    }


    public double getY() {
        return y;
    }


    public int getR() {
        return r;
    }

    public int getRank() {
        return rank;
    }

    /**
     * Mijenjanje x i y kordinata u ovisnosti promjene dx i dy
     * postavljaju se granice tako da neprijatelj ne moze izaci s ekrana
     *u slučaju da dođe do vijednosti di je x kordinata manja od širine objekta i promjena vrijednosti
     * x je manji od nula, znači da se kreće skroz izvan ekrana, mijenja se vrijednost dx u negativan dx, tj
     * promjena preznaka
     * što znaci da se promjeni kut kretnje i dobiva se odbijanje od krajeva ekrana
     * ista stvar je s y i dy i u slučaju da je x kordinata veća od širine ekrana -r postavlja se ista stvar
     * da dx i dy promjene preznak i dobivamo odbijanje od ekrana
     */
    public void update() {
        x += dx;
        y += dy;

        if (x < r && dx < 0)
            dx = -dx;
        if (y < r && dy < 0)
            dy = -dy;
        if (x > IgraPanel.sirina - r && dx > 0)
            dx = -dx;
        if (y > IgraPanel.visina - r && dy > 0)
            dy = -dy;
    }

    /**
     * Crta neprijatelje ne  u obliku kruga, i postavlja tamniji obrub
     * @param g Graphics2D iz IgraPanela koji crta grafiku na ekran
     */
    public void draw(Graphics2D g) {

        g.setColor(color);
        g.fillOval((int) (x - r), (int) (y - r), 2 * r, 2 * r);
        g.setStroke(new BasicStroke(3));
        g.setColor(color.darker());
        g.drawOval((int) (x - r), (int) (y - r), 2 * r, 2 * r);
        g.setStroke(new BasicStroke(1));
    }

    /**
     * hitted klasa radi tako da u slučaju da je postavljena boja u prvom trenu jednaka
     * žutoj koja je prva boja i da nije nitko još pogodio objekt, iduci put kad netko pogodi objekt
     * da se ne prišalta na žutu već na iduću koja je crvena
     * a u suprotnom slučaju šalta boje svaki put kad se izvede ova metoda na žutu, crvenu, plavu, i zelenu
     *
     */
    public void hit() {
        if (color.equals(Color.YELLOW) && hitted == 0)
            hitted++;
        hitted++;

        if (hitted == 1) {
            color = Color.YELLOW;
        } else if (hitted == 2) {
            color = Color.red;
        } else if (hitted == 3) {
            color = Color.BLUE;

        } else if (hitted == 4) {
            color = Color.GREEN;
            hitted = 0;
        }

    }

    /**
     * Provjerava da li su boje neprijatelja iste s bojom ovog objekta
     * @param o drugi objekt koji sadrži boju
     * @return vraća istinu ili laž ovisno jesu li boje objeta iste
     */
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EnemyCircle that = (EnemyCircle) o;

        return color.equals(that.color);
    }

    /**
     * vraća hashCode objekta definiranog bojom
     * @return
     */
    public int hashCode() {
        return color.hashCode();
    }
}

