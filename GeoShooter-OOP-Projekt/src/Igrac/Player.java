package Igrac;

import MainProzor.IgraPanel;

import java.awt.*;

/**
 * Created by Mario Tucak on 6.5.2017..
 *Definira se igrac koji se kreće po x i y kordinatama pri zadanoj brzini,
 * Definira se broj zivota sto igrac moze imati, te pucanje i koliko metaka u 1s moze ispaliti
 * I postavlja se logika u slucaju da je igrac pogođen da ne može biti ponovno pogođen određeno vrijeme
 * u ovom slučaju 1.5s
 */
public class Player {


    private int x; // x kordianta
    private int y; // y kordinata
    private int r; // radius

    private int dx; // promjena x-a
    private int dy; // promjena y-a
    private int speed; // brzina
    private int zivoti; // zivoti igraca


    private boolean pucanje;
    private long pucanjeTimer;
    private long pucanjeDelay;

    private boolean up, down, left, right;

    //Boje igraca
    private Color boja1;
    private Color boja2;

    //stanja igraca
    private boolean pogoden;
    private long pogodenTimer;

    //score
    public int score;
    //username Igrac
    private String username;

    /**
     * Inicijalizacija varijabli
     * zivoti se postavljaju na broj zivota koliko ce igrac imati
     * x i y se pozicinira na sredinu ekrana
     * velicina odnosno radius se postavlja na zeljenu u ovom slucaju 10
     * dx i dy su nula, jer jos nema promjene varijabli x i y
     * brzina kretanja igraca je 5
     *
     */
    public Player() {

        init();
    }

    private void init(){
        zivoti = 3;
        boja1 = Color.WHITE;
        boja2 = Color.RED;
        pucanje = false;
        pucanjeTimer = System.nanoTime();
        pucanjeDelay = 350; // 350 miliSekundi
        x = IgraPanel.sirina / 2; //pozicioniraj na pola x-a
        y = IgraPanel.sirina / 2; // pozicioniraj na pola y-a
        r = 10; //velicina playera
        dx = 0;
        dy = 0;
        speed = 5;
        pogoden=false;
        pogodenTimer=0;
        score=0;
    }
    public int getScore(){

        return score;
    }
    public int getX() {

        return x;
    }

    public int getY() {

        return y;
    }

    public int getR() {
        return r;
    }
    public String getUsername(){
        return username;
    }
    public void setUsername(String username){
       this.username=username;
    }

    public int getZivoti() {

        return zivoti;
    }
    public void setScore(int score){

        this.score+=score;
    }

    public void setUp(boolean up) {

        this.up = up;
    }

    public void setDown(boolean down) {

        this.down = down;
    }

    public void setLeft(boolean left) {

        this.left = left;
    }

    public void setRight(boolean right) {

        this.right = right;
    }

    public void setFiring(boolean fire) {

        pucanje = fire;
    }
    public boolean isPogoden(){

        return pogoden;
    }
    /**Oduzima živote od ukupnog broja
     * postavlja pogoden na istinitu koja aktivira stanje u kojemu ne mozemo umrijeti
     * i postavlja timer na trenutno vrijeme, te od ovog vremena ce se određivati koliko je ostalo jos
     * u stanju u kojemu ne mozemo umrijeti
     * */
    public void hitted(){
        zivoti--;
        pogoden=true;
        pogodenTimer=System.nanoTime();
    }

    /**
     * Zadužen za logiku igraca,
     * u slučaju da je jedan od predefiniranih tipaka pritisnuta u ovom slučaju
     * strelice gore, dolje, ljevo i desno koje su definirane boolean varijablama
     * mijenjaju se vrijednosti dx i dy ovisno o brzini
     * i pridružuju se vrijednostima x i y i time dobivamo lokaciju
     * ovaj metoda se poziva u gameUpdate() u IgraPanelu zaci odvija se 60 puta u sekundi
     * Postavljene su granice na širinu-r i dužinu - r ekrana,
     * ovisno o lokaciji i poslje toga postavljamo derivacije na 0
     * jer da nije toga, lik nebi nikada stao vec bi letao po mapi svaki put kad bi bilo koju od strelica
     * pritisnuli
     * U lučaju da je pucanje uvjet ispunjen stvara se lista metaka , s tim da postovi timer koji
     * određuje koliko ce se metaka stvoriti u određeno vrijeme
     */
    public void update() {
        if (up) {
            dy -= speed;
        }
        if (down) {
            dy += speed;
        }
        if (left) {
            dx -= speed;
        }
        if (right) {
            dx += speed;
        }
        /*Mijenjaj poziciju ovisno od dx i dy*/
        x += dx;
        y += dy;
        /*Postavavljamo granice kretnje*/
        if (x < r)
            x = r;  //lijeve granice
        if (y < r)
            y = r; // gornje granice
        if (x > IgraPanel.sirina - r) { //desne granice
            x = IgraPanel.sirina - r;
        }
        if (y > IgraPanel.visina - r) { //donje granice
            y = IgraPanel.visina - r;
        }
        dx = 0;
        dy = 0;
        if (pucanje) {
            long prosloV = (System.nanoTime() - pucanjeTimer) / 1000000;
            if (prosloV > pucanjeDelay) {
                IgraPanel.bullets.add(new Bullet(270, x, y));
                pucanjeTimer = System.nanoTime();
            }
        }
        /* prakticki odredujemo koliko ce igrac nece moc bit pogoden */
        long prosloV=(System.nanoTime()-pogodenTimer)/1000000;
        if(prosloV>1500){
            pogoden=false;
            pogodenTimer=0;
        }
    }

    /**
     * Crta igrača sa svim parametrima
     * @param g objekt tipa Graphics2D koji je zaduzen za sve graficke elemente i crtanja
     */
    public void draw(Graphics2D g) {
        drawPlayer(g);
        drawZivoti(g);
        drawScore(g);
    }

    private void drawPlayer(Graphics2D g){
        if(pogoden) {
            g.setColor(boja2);
            g.fillPolygon(new int[]{x - r, x, x + r}, new int[]{y + 10, y - 10, y + 10}, 3);
            g.setStroke(new BasicStroke(3));
            g.setColor(boja2.darker());
            g.drawPolygon(new int[]{x - r, x, x + r}, new int[]{y + r, y - r, y + r}, 3);
            g.setStroke(new BasicStroke(1));
        }
        else{
            g.setColor(boja1);
            g.fillPolygon(new int[]{x - r, x, x + r}, new int[]{y + 10, y - 10, y + 10}, 3);
            g.setStroke(new BasicStroke(3));
            g.setColor(boja1.darker());
            g.drawPolygon(new int[]{x - r, x, x + r}, new int[]{y + r, y - r, y + r}, 3);
            g.setStroke(new BasicStroke(1));
        }
    }
    private void drawZivoti(Graphics2D g){
        for(int i=0;i<zivoti;i++){
            g.setColor(Color.RED);
            g.fillRect(20+(16*i),20,8*2,8*2);
            g.setStroke(new BasicStroke(3));
            g.setColor(Color.RED.darker());
            g.drawRect(20+(16*i),20,8*2,8*2);
            g.setStroke(new BasicStroke(1));
        }
    }
    private void drawScore(Graphics2D g){
        g.setFont(new Font("Arial", Font.BOLD,14));
        String s="--S C O R E--  "+score;
        g.setColor(Color.BLACK);
        g.drawString(s,IgraPanel.sirina-s.length()-100,40);
    }
}
