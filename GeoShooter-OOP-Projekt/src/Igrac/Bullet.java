package Igrac;

import MainProzor.IgraPanel;

import java.awt.*;

/**
 * Created by Mario Tucak on 8.5.2017..
 * Klasa Bullet napravljena je s logikom da uzim lokaciju igrača i na njegovu mjestu prema gore stvara metke
 * u slučaju prijelaza granica, dužine i širine ekrana briše se metak
 * Zbog veće preciznosti definirane su x y dx i dy varijable kao double
 * boja1 određuje boju metka, dok speed  određuje brzinu kretnje metka, a radiani su kut kojim metak se kreće
 *  */
public class Bullet {
    private double x;
    private double y;
    private int r;
    private double dx;
    private double dy;
    private double radian;
    private double speed;
    private Color boja1;

    /**
     * U konstruktoru se postavljaju vrijednosti x i y na vrijednosti player x i y
     * posatavlja se radius odnosno širina
     * određuje se kut smijera kretnje metka     *
     * Postvalja se brzina na 13 i pridodaje se vrijednostim dx i dy uz to da se
     * dx postavim cos(radian)*speed (u ovom slucaju gdje je kut 270 vrijednost ovoga ce biti 0 ali
     * za promjenu kuta, mijenja se promjena dx odnosno promjena vrijednosti x)
     * ali za promjenu kuta se može mijenjati,
     * dok dy postavim na sin(radian)*speed ( u ovom slucaju ce to biti sin(radina) je 1 to znaci da ce biti 13)
     * boja1 se postavlja na zeljenu boju metka
     * @param kut kut smijera kako ce se kretati metak
     * @param x prima se x kordinata na kojoj je igrac
     * @param y prima se y kordinata na kojoj je igrac
     */
    public Bullet(double kut,int x, int y){
        this.x=x;
        this.y=y;
        r=2;
        radian=Math.toRadians(kut);
        speed=13;
        dx=Math.cos(radian)*speed;
        dy=Math.sin(radian)*speed;

        boja1= Color.BLACK;

    }
    public double getX() {
        return x;
    }


    public double getY() {
        return y;
    }


    public int getR() {
        return r;
    }

    /**
     * Glavna logika metka, mijenjaju se x i y kordinate u ovisnosti o dx i dy
     * postavljaju se granice kada metak prijeđe ekran da se povratna vrijednost postavi na true
     * i da se izbrise iz polja metaka koji se nalazi u IgraPanelu
     * @return vraća se vrijednost ture u slucaju da je metak presao širinu  il ivisinu ekrana
     * a vraća se false u slucaju da nije
     *
     */
    public boolean update(){
        x+=dx;
        y+=dy;

        if(x<-r || x> IgraPanel.sirina+r || y<-r || y>IgraPanel.sirina+r){
            return true;
        }

        return false;
    }

    /**
     * Postavlja boju metka na boju definiranu u clasi
     * CrtaMetak na ekran lokaciji y-r,x-r, i sirine 2*r i duzine 2*r,
     * @param g objekt tipa Graphics2D koji je zaduzen za sve graficke elemente i crtanja
     */
    public void draw(Graphics2D g){
        g.setColor(boja1);
        g.fillOval((int)(x-r),(int)(y-r),2*r,2*r);
    }

}
