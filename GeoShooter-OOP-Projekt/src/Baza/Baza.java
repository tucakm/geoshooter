package Baza;



import java.util.Map;

/**
 * Created by Mario Tucak on 21.5.2017..
 * Interface baze koji ima mogućnosti čitanja i dodavanja elemenata u bazu
 */
public interface Baza {

    Map<String,Integer> citaj();
    void dodaj(long score, String username);


}
