package Baza;

import java.io.*;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Mario Tucak on 21.5.2017..
 * Baza podataka datotekom koja implementira Bazu
 * Definira se datoteka i mapa Stringova  i Ingegera koji prestavljaju highscore
 */
public class BazaFile implements Baza {
    private File file;
    private Map<String,Integer> highScore;

    /**
     * datotaeka se postavlja na geoShooter.txt, u slučaju da ne postoji radi se nova
     */
    public BazaFile(){
        file=new File("geoShooter.txt");
        if(!file.exists()){
            try{
                file.createNewFile();
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /**
     * Čita iz fajla , sprema ih u string i splita do svakog ;, te ih sprema u mapu
     * @return se mapa u kojoj se key username String a score je value
     */
    public Map<String,Integer> citaj() {
        highScore = new TreeMap<>();
        String c;
        try(BufferedReader dat = new BufferedReader(new
                FileReader(file))) {
            while ((c = dat.readLine()) != null) {
                String[] dijelovi = c.split(";");
                String username=dijelovi[0];
                int score=Integer.valueOf(dijelovi[1]);
                highScore.put(username,score);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return highScore;
    }

    /**
     * Dodaje se na kraj mape vrijednosti parametra score i username razdvojeni ;
     * @param score integer koji prestavlja vrijednosti ostvarene u igrici
     * @param username je ime korsinika koji se šalje bazi i sprema u fajl
     */
    public void dodaj(long score, String username ) {

        try(PrintWriter dat = new
                PrintWriter(new FileWriter(file,true))){
            dat.println(username+";"+score);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
